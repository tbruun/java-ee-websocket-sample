package se.thomasbruun;

import javax.inject.Inject;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;

// JavaScript to access from a WebSocket capable browser would be:  ws://<Host Name>:<port>/<Context-Root>/SimpleAnnotated
@ServerEndpoint("/websocket")
public class WebSocketEndpoint {

    @Inject
    private TimeNotifier    timeNotifier;
    private Session         currentSession;
    private int             count;

    @OnOpen
    public void onOpen(Session session, EndpointConfig ec) {
        currentSession = session;
        timeNotifier.addClient(session);
    }

    @OnMessage
    public void receiveMessage(String message) {
        try {
            //basic echo server - send same message back
            currentSession.getBasicRemote().sendText(message + " || Messages recieved: " + ++count);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        timeNotifier.removeClient(session);
    }
}
