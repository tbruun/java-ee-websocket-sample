package se.thomasbruun;

import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Holds a list of websocket clients and sends them the current date-time every second.
 */
@Singleton
public class TimeNotifier {

    private List<Session> clients = new ArrayList<>();

    public void addClient(Session client) {
        clients.add(client);
    }

    public void removeClient(Session client) {
        clients.remove(client);
    }

    @Schedule(second= "*", minute = "*", hour = "*", persistent = false)
    public void messageClients() {
        clients.forEach(client -> {
            try {
                client.getBasicRemote().sendText("Server time: " + new Date().toString());
            } catch (IOException ignored) {}
        });
    }

}
