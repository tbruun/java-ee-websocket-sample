var webSocket = new WebSocket('ws://localhost:8080/websocket/websocket');

webSocket.onerror = function (event) {
    document.getElementById('status').innerHTML = 'Error: ' + event.data;
};

webSocket.onopen = function () {
    document.getElementById('status').innerHTML = 'Connection established';
};

webSocket.onmessage = function (event) {
    document.getElementById('messages').innerHTML = event.data;
};

function send() {
    var txt = document.getElementById('inputmessage').value;
    webSocket.send(txt);
}